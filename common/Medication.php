<?php
/**
 * Medication.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'DB.php';

class Medication extends DBTable {

  # GET QUERIES
  const QUERY_FROM_CLIENT = "SELECT * FROM Clients_Medications WHERE client_id = :client_id;";
  const QUERY_FROM_ID = "SELECT * FROM Clients_Medications WHERE id = :id LIMIT 1;";

  const QUERY_UPDATE = "UPDATE Clients_Medications SET 
  medication = :medication,
  dosage = :dosage,
  frequency = :frequency,
  time_taken = :time_taken
  WHERE id = :id";

  protected $id,
    $client_id,
    $medication,
    $dosage,
    $frequency,
    $time_taken,
    $active;

  /**
   * @param int $client_id
   * @return Medication[]
   */
  public static function query_from_client( int $client_id ): array {
    return self::queryAll( self::QUERY_FROM_CLIENT, array( ':client_id' => $client_id ) );
  }


  /**
   * @param int $id
   * @return Medication|bool
   */
  public static function query_from_id( int $id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':id' => $id ) );
  }

//  function insert(): bool {
//  }
//
  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':medication', $this->medication, PDO::PARAM_STR );
      $stmt->bindParam( ':dosage', $this->dosage, PDO::PARAM_STR );
      $stmt->bindParam( ':frequency', $this->frequency, PDO::PARAM_STR );
      $stmt->bindParam( ':time_taken', $this->time_taken, PDO::PARAM_STR );
      $stmt->bindParam( ':id', $this->id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId( int $client_id ): void {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getMedication(): string {
    return $this->medication;
  }

  /**
   * @param string $medication
   */
  public function setMedication( string $medication ): void {
    $this->medication = $medication;
  }

  /**
   * @return string
   */
  public function getDosage(): string {
    return $this->dosage;
  }

  /**
   * @param string $dosage
   */
  public function setDosage( string $dosage ): void {
    $this->dosage = $dosage;
  }

  /**
   * @return string
   */
  public function getFrequency(): string {
    return $this->frequency;
  }

  /**
   * @param string $frequency
   */
  public function setFrequency( string $frequency ): void {
    $this->frequency = $frequency;
  }

  /**
   * @return string
   */
  public function getTimeTaken(): string {
    return $this->time_taken;
  }

  /**
   * @param string $time_taken
   */
  public function setTimeTaken( string $time_taken ): void {
    $this->time_taken = $time_taken;
  }

  /**
   * @return bool
   */
  public function isActive(): bool {
    return $this->active;
  }

  /**
   * @param bool $active
   */
  public function setActive( bool $active ): void {
    $this->active = $active;
  }
}