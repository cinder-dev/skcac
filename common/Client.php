<?php
/**
 * #####################################################################################################################
 * Client.php
 *
 * Queries can be executed using the static methods to get data, or insert data once populated.
 *
 * Author: Caleb Snoozy
 * Date: 2/15/18
 * #####################################################################################################################
 */

require_once 'DBTable.php';
require_once 'Contact.php';
require_once 'DietRestriction.php';
require_once 'EmergencyContact.php';
require_once 'MedicalAlert.php';
require_once 'Medication.php';
require_once 'PhysicalLimitation.php';
require_once 'DB.php';

/** Class Client PHP Representation of the Clients Table. */
class Client extends DBTable {

### GET QUERIES ########################################################################################################

  /** Request all clients from the database, does not check active status. */
  const QUERY_ALL = "SELECT * FROM Clients;";

  /** Request a client from the database via ID. */
  const QUERY_FROM_ID = "SELECT * FROM Clients WHERE client_id = :client_id LIMIT 1;";

### POST QUERIES #######################################################################################################

  /** Update a client's data. */
  const QUERY_UPDATE = "UPDATE Clients SET 
  middle_name = :middle_name, 
  phone = :phone, 
  address = :address,
  address_city = :address_city,
  address_zip = :address_zip, 
  address_state = :address_state, 
  last_update = :last_update,
  active_client = :active_client
  WHERE client_id = :client_id;";

  /** Insert a Client based of this object. */
  const QUERY_INSERT = "INSERT INTO Clients (
    middle_name, 
    phone, 
    address, 
    address_city, 
    address_zip, 
    address_state, 
    last_update, 
    active_client
    ) VALUE (
    :middle_name, 
    :phone, 
    :address, 
    :address_city, 
    :address_zip, 
    :address_state, 
    :last_update, 
    :active_client
    )";

  protected $client_id,
    $account_id,
    $middle_name,
    $phone,
    $address,
    $address_city,
    $address_zip,
    $address_state,
    $last_update,
    $active_client;


  /**
   * @param $client_id int Client Id.
   * @return Client|bool
   */
  static function query_from_id( int $client_id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':client_id' => $client_id ) );
  }


  /** @return Client[] Requests All Clients. */
  public static function query_All() {
    return self::queryAll( self::QUERY_ALL );
  }

  /**
   * Insert this client to the database.
   *
   * @return bool True if successful.
   */
  public function insert(): bool {

  }

  /**
   * Update matching client in the database.
   *
   * @return bool True if successful.
   */
  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':middle_name', $this->middle_name, PDO::PARAM_STR );
      $stmt->bindParam( ':phone', $this->phone, PDO::PARAM_INT );
      $stmt->bindParam( ':address', $this->address, PDO::PARAM_STR );
      $stmt->bindParam( ':address_city', $this->address_city, PDO::PARAM_STR );
      $stmt->bindParam( ':address_zip', $this->address_zip, PDO::PARAM_STR );
      $stmt->bindParam( ':address_state', $this->address_state, PDO::PARAM_STR );
      $stmt->bindParam( ':last_update', $this->last_update, PDO::PARAM_STR );
      $stmt->bindParam( ':active_client', $this->active_client, PDO::PARAM_BOOL );
      $stmt->bindParam( ':client_id', $this->client_id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return Contact[] This clients contacts.
   */
  public function get_contacts(): array {
    return Contact::query_from_client( $this->client_id );
  }


  /**
   * @param int $id Specific Contacts ID.
   * @return Contact Specified Contact.
   */
  public function get_contact( int $id ): Contact {
    return Contact::query_from_id( $id );
  }

  /**
   * @return EmergencyContact[] This Clients Emergency Contacts.
   */
  public function get_emergency_contacts(): array {
    return EmergencyContact::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Emergency Contacts ID.
   * @return EmergencyContact Specified Emergency Contact.
   */
  public function get_emergency_contact( int $id ): EmergencyContact {
    return EmergencyContact::query_from_id( $id );
  }

  /**
   * @return MedicalAlert[] Clients Medical Alerts.
   */
  public function get_medical_alerts(): array {
    return MedicalAlert::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Alert ID.
   * @return MedicalAlert Specified Alert.
   */
  public function get_medical_alert( int $id ): MedicalAlert {
    return MedicalAlert::query_from_id( $id );
  }

  /**
   * @return PhysicalLimitation[] Clients Physical Limitations.
   */
  public function get_physical_limitations(): array {
    return PhysicalLimitation::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Limitations ID.
   * @return PhysicalLimitation Specified Limitation.
   */
  public function get_physical_limitation( int $id ): PhysicalLimitation {
    return PhysicalLimitation::query_from_id( $id );
  }

  /**
   * @return DietRestriction[] Clients Diet Restrictions.
   */
  public function get_diet_restrictions(): array {
    return DietRestriction::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Restrictions ID.
   * @return DietRestriction Specified Restriction.
   */
  public function get_diet_restriction( int $id ): DietRestriction {
    return DietRestriction::query_from_id( $id );
  }

  /**
   * @return Medication[] Clients Medications.
   */
  public function get_medications(): array {
    return Medication::query_from_client( $this->client_id );
  }

  /**
   * @param int $id Specific Medications ID.
   * @return Medication Specified Medication.
   */
  public function get_medication( int $id ): Medication {
    return Medication::query_from_id( $id );
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @return int
   */
  public function getAccountId(): int {
    return $this->account_id;
  }

  /**
   * @return string
   */
  public function getMiddleName(): string {
    return $this->middle_name;
  }

  /**
   * @param string $middle_name
   */
  public function setMiddleName( string $middle_name ): void {
    $this->middle_name = $middle_name;
  }

  /**
   * @return int
   */
  public function getPhone(): int {
    return $this->phone;
  }

  /**
   * @param int $phone
   */
  public function setPhone( int $phone ): void {
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getAddress(): string {
    return $this->address;
  }

  /**
   * @param string $address
   */
  public function setAddress( string $address ): void {
    $this->address = $address;
  }

  /**
   * @return string
   */
  public function getAddressCity(): string {
    return $this->address_city;
  }

  /**
   * @param string $address_city
   */
  public function setAddressCity( string $address_city ): void {
    $this->address_city = $address_city;
  }

  /**
   * @return string
   */
  public function getAddressZip(): string {
    return $this->address_zip;
  }

  /**
   * @param string $address_zip
   */
  public function setAddressZip( string $address_zip ): void {
    $this->address_zip = $address_zip;
  }

  /**
   * @return string
   */
  public function getAddressState(): string {
    return $this->address_state;
  }

  /**
   * @param string $address_state
   */
  public function setAddressState( string $address_state ): void {
    $this->address_state = $address_state;
  }

  /**
   * @return string
   */
  public function getLastUpdate(): string {
    return $this->last_update;
  }

  /**
   * @param string $last_update
   */
  public function setLastUpdate( string $last_update ): void {
    $this->last_update = $last_update;
  }

  /**
   * @return bool
   */
  public function isActiveClient(): bool {
    return $this->active_client;
  }

  /**
   * @param bool $active_client
   */
  public function setActiveClient( bool $active_client ): void {
    $this->active_client = $active_client;
  }

}