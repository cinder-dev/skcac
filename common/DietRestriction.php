<?php
/**
 * DietRestriction.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'DB.php';

class DietRestriction extends DBTable {

  # GET QUERIES
  const QUERY_FROM_CLIENT = "SELECT * FROM Clients_Diet_Restrictions WHERE client_id = :client_id;";
  const QUERY_FROM_ID = "SELECT * FROM Clients_Diet_Restrictions WHERE id = :id LIMIT 1;";

  const QUERY_UPDATE = "UPDATE Clients_Diet_Restrictions SET 
  restriction = :restriction
  WHERE id = :id;";

  protected $id,
    $client_id,
    $restriction,
    $active;


  /**
   * @param int $client_id
   * @return DietRestriction[]
   */
  public static function query_from_client( int $client_id ): array {
    return self::queryAll( self::QUERY_FROM_CLIENT, array( ':client_id' => $client_id ) );
  }


  /**
   * @param int $id
   * @return DietRestriction|bool
   */
  public static function query_from_id( int $id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':id' => $id ) );
  }

//  function insert(): bool {
//
//  }

  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':restriction', $this->restriction, PDO::PARAM_STR );
      $stmt->bindParam( ':id', $this->id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId( int $client_id ): void {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getRestriction(): string {
    return $this->restriction;
  }

  /**
   * @param string $restriction
   */
  public function setRestriction( string $restriction ): void {
    $this->restriction = $restriction;
  }

  /**
   * @return bool
   */
  public function isActive(): bool {
    return $this->active;
  }

  /**
   * @param bool $active
   */
  public function setActive( bool $active ): void {
    $this->active = $active;
  }
}