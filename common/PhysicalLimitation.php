<?php
/**
 * PhysicalLimitation.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'DB.php';

class PhysicalLimitation extends DBTable {

  # GET QUERIES
  const QUERY_FROM_CLIENT = "SELECT * FROM Clients_Physical_Limitations WHERE client_id = :client_id;";
  const QUERY_FROM_ID = "SELECT * FROM Clients_Physical_Limitations WHERE id = :id LIMIT 1;";

  const QUERY_UPDATE = "UPDATE Clients_Physical_Limitations SET 
  limitation = :limitation
  WHERE id = :id;";

  protected $id,
    $client_id,
    $limitation,
    $active;

  /**
   * @param int $client_id
   * @return PhysicalLimitation[]
   */
  public static function query_from_client( int $client_id ): array {
    return self::queryAll( self::QUERY_FROM_CLIENT, array( ':client_id' => $client_id ) );
  }


  /**
   * @param int $id
   * @return PhysicalLimitation|bool
   */
  public static function query_from_id( int $id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':id' => $id ) );
  }

//  function insert(): bool {
//  }
//
  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':limitation', $this->limitation, PDO::PARAM_STR );
      $stmt->bindParam( ':id', $this->id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getId(): int {
    return $this->id;
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId( int $client_id ): void {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getLimitation(): string {
    return $this->limitation;
  }

  /**
   * @param string $limitation
   */
  public function setLimitation( string $limitation ): void {
    $this->limitation = $limitation;
  }

  /**
   * @return bool
   */
  public function isActive(): bool {
    return $this->active;
  }

  /**
   * @param bool $active
   */
  public function setActive( bool $active ): void {
    $this->active = $active;
  }
}