<?php
/**
 * Contact.php
 *
 * description
 *
 * Author: Caleb Snoozy
 * Date: 2/17/18
 */

require_once 'DBTable.php';
require_once 'DB.php';

class Contact extends DBTable {

  # GET QUERIES
  const QUERY_FROM_CLIENT = "SELECT * FROM Clients_Contacts WHERE client_id = :client_id;";
  const QUERY_FROM_ID = "SELECT * FROM Clients_Contacts WHERE contact_id = :contact_id LIMIT 1;";

  const QUERY_UPDATE = "UPDATE Clients_Contacts SET
    first_name = :first_name,
    last_name = :last_name,
    relation = :relation,
    email = :email,
    phone = :phone,
    address = :address,
    address_city = :address_city,
    address_zip = :address_zip,
    address_state = :address_state
  WHERE contact_id = :contact_id";

  protected $contact_id,
    $client_id,
    $first_name,
    $last_name,
    $relation,
    $email,
    $phone,
    $address,
    $address_city,
    $address_zip,
    $address_state,
    $active_contact;

  /**
   * @param int $client_id
   * @return Contact[]
   */
  public static function query_from_client( int $client_id ): array {
    return self::queryAll( self::QUERY_FROM_CLIENT, array( ':client_id' => $client_id ) );
  }

  /**
   * @param int $id
   * @return Contact|bool
   */
  static function query_from_id( int $id ) {
    return self::queryOne( self::QUERY_FROM_ID, array( ':contact_id' => $id ) );
  }

//  function insert(): bool {
//
//  }

  public function update(): bool {
    try {
      $db = new DB();
      $stmt = $db->prepare( self::QUERY_UPDATE );
      $stmt->bindParam( ':first_name', $this->first_name, PDO::PARAM_STR );
      $stmt->bindParam( ':last_name', $this->last_name, PDO::PARAM_STR );
      $stmt->bindParam( ':relation', $this->relation, PDO::PARAM_STR );
      $stmt->bindParam( ':email', $this->email, PDO::PARAM_STR );
      $stmt->bindParam( ':phone', $this->phone, PDO::PARAM_INT );
      $stmt->bindParam( ':address', $this->address, PDO::PARAM_STR );
      $stmt->bindParam( ':address_city', $this->address_city, PDO::PARAM_STR );
      $stmt->bindParam(':address_zip', $this->address_zip, PDO::PARAM_STR);
      $stmt->bindParam( ':address_state', $this->address_state, PDO::PARAM_STR );
      $stmt->bindParam( ':contact_id', $this->contact_id, PDO::PARAM_INT );
      return $stmt->execute();
    } catch ( PDOException $ex ) {
      die( $ex->getMessage() . PHP_EOL . $ex->getTraceAsString() );
    }
  }

  /**
   * @return int
   */
  public function getContactId(): int {
    return $this->contact_id;
  }

  /**
   * @return int
   */
  public function getClientId(): int {
    return $this->client_id;
  }

  /**
   * @param int $client_id
   */
  public function setClientId( int $client_id ): void {
    $this->client_id = $client_id;
  }

  /**
   * @return string
   */
  public function getFirstName(): string {
    return $this->first_name;
  }

  /**
   * @param string $first_name
   */
  public function setFirstName( string $first_name ): void {
    $this->first_name = $first_name;
  }

  /**
   * @return string
   */
  public function getLastName(): string {
    return $this->last_name;
  }

  /**
   * @param string $last_name
   */
  public function setLastName( string $last_name ): void {
    $this->last_name = $last_name;
  }

  /**
   * @return string
   */
  public function getRelation(): string {
    return $this->relation;
  }

  /**
   * @param string $relation
   */
  public function setRelation( string $relation ): void {
    $this->relation = $relation;
  }

  /**
   * @return string
   */
  public function getEmail(): string {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail( string $email ): void {
    $this->email = $email;
  }

  /**
   * @return int
   */
  public function getPhone(): int {
    return $this->phone;
  }

  /**
   * @param int $phone
   */
  public function setPhone( int $phone ): void {
    $this->phone = $phone;
  }

  /**
   * @return string
   */
  public function getAddress(): string {
    return $this->address;
  }

  /**
   * @param string $address
   */
  public function setAddress( string $address ): void {
    $this->address = $address;
  }

  /**
   * @return string
   */
  public function getAddressCity(): string {
    return $this->address_city;
  }

  /**
   * @param string $address_city
   */
  public function setAddressCity( string $address_city ): void {
    $this->address_city = $address_city;
  }

  /**
   * @return mixed
   */
  public function getAddressZip() {
    return $this->address_zip;
  }

  /**
   * @param mixed $address_zip
   */
  public function setAddressZip( $address_zip ): void {
    $this->address_zip = $address_zip;
  }

  /**
   * @return string
   */
  public function getAddressState(): string {
    return $this->address_state;
  }

  /**
   * @param string $address_state
   */
  public function setAddressState( string $address_state ): void {
    $this->address_state = $address_state;
  }

  /**
   * @return bool
   */
  public function isActiveContact(): bool {
    return $this->active_contact;
  }

  /**
   * @param bool $active_contact
   */
  public function setActiveContact( bool $active_contact ): void {
    $this->active_contact = $active_contact;
  }
}