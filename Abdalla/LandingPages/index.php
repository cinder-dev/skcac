<?php
// index page

/*session_start();*/
if ( !empty( $_SESSION[ 'user' ] ) ) {
    header( "Location: ../dashboard" );
    die( "Redirecting to ../dashboard" );
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require '../common/index.php'; // Connect to index.
    require '../common/Account.php'; //Connect to Account
    /*require '../LandingPages/ForgotPassword';*/

    if ( !empty( $_POST ) ) {
        $submitted_email = $_POST[ 'email' ];
        $submitted_password = $_POST[ 'password' ];

        $account = Account::query_from_email( $submitted_email );
        if ( !empty( $account ) ) {

            for ( $round = 0; $round < 65537; $round++ ) {
                $submitted_password = hash( 'sha256', $submitted_password . $account->getSalt() );
            }

            if ( $submitted_password === $account->getPassword() ) {
                $login_ok = true;
            }

            if ( $login_ok ) {
                $_SESSION[ 'user' ] = $account->getEmail();
                $_SESSION[ 'staff' ] = $account->isStaff();
                $_SESSION[ 'admin' ] = $account->isAdmin();

                // check if staff login redirect to staff dashboard ELSE if user redirect to user dashboard
                if ($account->isStaff()){
                    header( "Location: /staff/dashboard/index.php" );
                    die( "Redirecting to /staff/dashboard/index.php" );
                } else {
                    header( "Location: ../Joseph/UserLanding.php" );
                    die( "Redirecting to ../Joseph/UserLanding.php" );
                }
            } else {
                printf( "Login Failed! Email or Password are incorrect" );
                $submitted_email = htmlentities( $_POST[ 'email' ], ENT_QUOTES, 'UTF-8' );
            }

        }
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sign In</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
    <style>
        body{
            font-family: 'Ubuntu Mono', monospace;
            font-size: 20px;
        }
    </style>
</head>
<body>
        <!--logo goes here-->
        <div class="container text-center">
            <br>
                <img src="http://aabdalla.greenriverdev.com/TeamSprintbacklog2/skcac%20header.png" alt="header">
        </div>
        <div class="container">
        <!--table goes here with message and colors-->
            <nav class="navbar bg-default">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link navbar-brand text-uppercase text-black" style="font-size: 45px; ">Sign in</a>
                    </li>
                </ul>
            </nav>
        </div>
        <div class="container">
            <form class="text-uppercase" method="post">
                <div class="form-group text-uppercase">
                    <br>
                    <input name="email" type="email" id="inputEmail" class="form-control" placeholder="EMAIL*" required>
                    <div class="invalid-feedback"> Invalid email or password </div>
                </div>
                <div class="form-group text-uppercase">
                    <br>
                    <input name="password" type="password" id="inputPassword" class="form-control" placeholder="PASSWORD" required>
                </div>
                <br>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div >
                            <input type="checkbox" aria-label="Checkbox for following text input">
                            <label> Remember me</label>
                        </div>
                    </div>
                </div>
                <button type="submit" value="SignIn" class="btn btn-success btn-lg btn-block text-uppercase">Sign in</button>
                <br>
                <div class="text-center">
                    <!--forgot password link onclick to be changed to official link-->
                    <button name="forgotPassword" type="button" id="forgotPassword" class="btn btn-warning btn-lg form-action" onclick="location.href='ForgotPassword.php'">Forgot Password</button>
                    <!--new user link to be changed to official link-->
                    <button id="newSignIn" name="newSignIn" class="btn btn-primary btn-lg form-action" onclick="location.href='register/register.php'"><strong>New? Sign Up</strong></button>
                </div>
            </form>
        </div>

    <!--footer starts here-->
    <div>
        <br>
        <!--footer goes here-->
        <table class="table table-info">
            <tbody>
            <tr>
                <th></th>
            </tr>
            <tr>
                <th scope="row"> SKCAC Industries and Employment Services</th>
            </tr>
            <tr>
                <th>Phone: 253-395-1240   TDD RELAY: 711</th>
            </tr>
            <tr>
                <th>19731 Russell Road South, Kent, Washington 98032-1117</th>
            </tr>
            <tr>
                <th> </th>
            </tr>
            <tr>
                <th> </th>
            </tr>
            </tbody>
        </table>
    </div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>