<?php

// put new password
// Check for form submission:
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require '../common/Account.php'; //Connect to Account

    if (!empty($_POST)) {
        $submitted_password = $_POST['password'];
        $confirm_password = $_POST['confirm password'];

        $submitted_email = $_POST[ 'email' ];

        $account = Account::query_from_email($submitted_email);

        // if input is not empty
        if (!empty($account)) {

            // check if email matches the database
            if ($submitted_email === $account->getEmail()  ) {
                $match_ok = true;
            }

            if ($match_ok) {
                $_SESSION[ 'user' ] = $account->getEmail();
                $_SESSION[ 'staff' ] = $account->isStaff();
                $_SESSION[ 'admin' ] = $account->isAdmin();

                //adding salt and hashing the password
                $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2137483647));
                $password = hash('sha256', $password . $salt);

                // Super hash the password
                for ($round = 0; $round < 65536; $round++) {
                    $password = hash('sha256', $password . $salt);}

                // update in the database with new password
                $q = "UPDATE ACCOUNT
                SET password = '$password'
                wHERE email = '$submitted_email' LIMIT 1";

                $r = @mysqli_query($dbc, $q); // Run the query.
                if ($r){
                    // check if staff login redirect to staff dashboard ELSE if user redirect to user dashboard
                    if ($account->isStaff()){
                        header( "Location: /staff/dashboard/index.php" );
                        die( "Redirecting to /staff/dashboard/index.php" );
                    } else {
                        $_SESSION['email'] = $submitted_email;
                        header( "Location: ../Joseph/UserLanding.php" );
                        die( "Redirecting to ../Joseph/UserLanding.php" );
                    }
                }
            }else {
                printf( "Login Failed! Email or Password are incorrect" );
                $submitted_email = htmlentities( $_POST[ 'email' ], ENT_QUOTES, 'UTF-8' );
            }
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>New Password</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/css/mdb.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu+Mono" rel="stylesheet">
    <style>
        body{
            font-family: 'Ubuntu Mono', monospace;
            font-size: 20px;
        }
    </style>
</head>

<body>
<!--############################################################  Navbar goes here ##################################################-->
<div>
    <nav class="navbar navbar-expand-lg navbar-light bg-light text-uppercase ">
        <img src="http://jacadevelopment.greenriverdev.com/Abdalla/LandingPages/mini_logo.png"  width="100px" height="60px" alt="logo" >
        <a class="navbar-brand text-black" href=""> </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-item nav-link active navbar-brand" href="https://www.skcacindustries.com">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link active navbar-brand" href="https://www.skcacindustries.com/employment-services/">Employment Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link active navbar-brand" href="https://www.skcacindustries.com/production-services/">Production Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link active navbar-brand" href="https://www.skcacindustries.com/donate/">Donate</a>
                </li>
                <li class="nav-item">
                    <a class="nav-item nav-link active navbar-brand" href="https://www.skcacindustries.com/resources-links/">Resources</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

<!--############################################################  Navbar ends here ##################################################-->

<!--############################################################ logo goes here ##################################################-->
<div class="container text-center">
    <br>
    <img src="http://jacadevelopment.greenriverdev.com/logo.png" alt="header">
</div>
<!--############################################################ logo ends here ##################################################-->

<div class="container">
        <!--table goes here with message and colors-->
    <div class="container">
        <label style="font-size: 80px">NEW PASSWORD</label>
    </div>
        <div class="container">
            <form class="pure-form">
                <div class="form-group text-uppercase">
                    <br>
                    <input type="email" class="form-control" id="password" placeholder="Email*" required >
                    <!--oninvalid="this.setCustomValidity('Please Enter valid email')" oninput="setCustomValidity('')"-->
                </div>
                <fieldset>
                    <div class="form-group text-uppercase">
                        <br>
                        <input type="password" class="form-control" id="password" placeholder="NEW PASSWORD*" required minlength="6">
                        <!--oninvalid="this.setCustomValidity('Please Enter valid password')" oninput="setCustomValidity('')"-->
                        <div class="invalid-feedback">
                            Please enter complex password. Minimum 6 characters
                        </div>
                    </div>
                    <div class="form-group text-uppercase">
                        <br>
                        <input type="password" class="form-control" id="confirm_password" placeholder="CONFIRM PASSWORD*" required>
                        <div class="invalid-feedback">
                            Passwords don't match
                        </div>
                    </div>
                    <br>
                    <button type="submit" id="myFinish" class="btn btn-success btn-lg btn-block text-uppercase" style="font-size: 20px"><strong>Finish</strong></button>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<!--footer starts here-->
    <?php
    require_once ('../register/footer.html');
    ?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<!--Script that checks to see if password matches-->
<!--<script>
    var password = document.getElementById("password")
        , confirm_password = document.getElementById("confirm_password");

    function validatePassword(){
        if(password.value != confirm_password.value) {
            confirm_password.setCustomValidity("Passwords Don't Match");
        } else {
            confirm_password.setCustomValidity('');
        }
    }

    password.onchange = validatePassword;
    confirm_password.onkeyup = validatePassword;
</script>-->
<!-- MDB core JavaScript -->
<!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.5.0/js/mdb.min.js"></script>-->
</body>
