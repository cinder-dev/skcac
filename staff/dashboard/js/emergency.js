$('#edit-contact').on('show.bs.modal', function (event) {
  let button = $(event.relatedTarget);
  let contact_id = button.data('contact-id');
  let url = new URL(window.location.href);
  let client_id = url.searchParams.get('client_id'); // From GET

  let modal = $(this);

  $.ajax({
    url: `/api/client/${client_id}/emergency_contact/${contact_id}`,
    dataType: 'json',
    success: function (data) {
      modal.find('#contact-edit-input-first-name').val(data['first_name']);
      modal.find('#contact-edit-input-last-name').val(data['last_name']);
      modal.find('#contact-edit-input-phone').val(data['phone']);
      modal.find('#contact-edit-input-alternate-phone').val(data['alternate_phone']);
      modal.find('#contact-edit-form').attr('action', '/api/client/' + client_id + '/emergency_contact/' + contact_id);
    }
  });
});

$(function () {
  $('[data-tooltip="tooltip"]').tooltip()
});