<?php
/**
 * index.php.old
 *
 * SKCAC Staff Portal
 *
 * Author: Caleb Snoozy
 * Date: 2/12/18
 */

if ( !session_start() )
  die( "Unable to start session!" );

if ( isset( $_SESSION[ 'user' ] ) ) {
  header( "Location: dashboard" );
  die( "Redirecting to dashboard" );
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>SKCAC Staff Portal</title>
  <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet"
        href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link rel="stylesheet"
        href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet"
        href="css/signin.css">
</head>

<body class="text-center">
<div class='container'>
  <form class="form-signin">
    <img class="mb-4"
         src="resources/skcaclogo.png"
         alt="logo"
         width="128"
         height="128">
    <h1 class="h3 mb-3 font-weight-normal">SKCAC Industries & Employment Services</h1>
    <a href="/"
       class="btn btn-lg btn-primary btn-block">Login</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2017-2018</p>
  </form>

  <?php require 'dashboard/components/footer.html' ?>
</div>

<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
</body>
</html>
