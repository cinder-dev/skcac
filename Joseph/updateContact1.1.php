<?php

require '/home/jacadeve/public_html/common/index.php';
require_once '/home/jacadeve/public_html/common/Client.php';
require_once '/home/jacadeve/public_html/common/Account.php';



/*if ( isset( $_SESSION[ 'update_status' ] ) ) {
    if ( $_SESSION[ 'update_status' ] === true ) {
        echo '<script language="javascript">';
        echo 'alert("Update Successful.")';
        echo '</script>';
    } else {
        echo '<script language="javascript">';
        echo 'alert("Update Failed.")';
        echo '</script>';
    }
    unset( $_SESSION[ 'update_status' ] );
}

if ( empty( $_GET ) ) {
    header( "Location: ../../dashboard" );
    die( "Relocating to ../../dashboard" );
}*/

$client_id = $_SESSION['clientid'];


$participant = Participant::query_from_client_id( $client_id );

$first_name = htmlentities( $participant->getFirstName(), ENT_QUOTES, 'UTF-8' );
$last_name = htmlentities( $participant->getLastName(), ENT_QUOTES, 'UTF-8' );
$middle_name = htmlentities( $participant->getMiddleName(), ENT_QUOTES, 'UTF-8' );
$full_name = htmlentities( $participant->getFirstName() . ' ' . $participant->getLastName(), ENT_QUOTES, 'UTF-8' );
$email = htmlentities( $participant->getEmail(), ENT_QUOTES, 'UTF-8' );
#$phone = htmlentities( $participant->getPhone(), ENT_QUOTES, 'UTF-8' );
$address = htmlentities( $participant->getAddress(), ENT_QUOTES, 'UTF-8' );
$address_city = htmlentities( $participant->getAddressCity(), ENT_QUOTES, 'UTF-8' );
$address_zip = htmlentities( $participant->getAddressZip(), ENT_QUOTES, 'UTF-8' );
$address_state = htmlentities( $participant->getAddressState(), ENT_QUOTES, 'UTF-8' );
$last_update = htmlentities( $participant->getLastUpdate(), ENT_QUOTES, 'UTF-8' );
$active_client = htmlentities( $participant->isActiveClient(), ENT_QUOTES, 'UTF-8' );

$emergency_contacts = $participant->get_emergency_contacts();

$id=null;
/**
 * @param $contact EmergencyContact
 */
function render_contact( $contact ) {
    echo "
<div class='col-lg-6'>
  <div class='card text-center'>
    <div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center h6 card-header'>
      <div>" . $contact->getFirstName() . " " . $contact->getLastName() . "</div>
      <div class='btn-toolbar mb-2 mb-md-0'>
        <div class='mr-2'>
          <button class='btn btn-sm btn-outline-danger' data-tooltip='tooltip' data-placement='top' title='Remove Medication'>
            <span class='d-none d-xl-inline'>Remove </span><span data-feather='delete'>X</span></button>
        </div>
      </div>
    </div>
    <div class='card-body'>
      <h6 class='card-title'>Primary Phone Number: " . render_phone_number( $contact->getPhone() ) . "</h6>
      <div>Alternate Phone Number: " . render_phone_number( $contact->getAlternatePhone() ) ."</div>
      <div></div>
    </div>
    <div class='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-right card-footer text-muted'>
      <div class='btn-toolbar mb-2 mb-md-0'>
        <div class='mr-2'>
          <button class='btn btn-sm btn-outline-secondary' data-toggle='modal' data-target='#edit-contact' 
          data-contact-id='" . $contact->getEmergencyContactId() . "' data-tooltip='tooltip' data-placement='top' title='Edit Emergency Contact' onclick=''
            <span class='d-none d-xl-inline'>Edit </span><span data-feather='edit'>Edit</span></button>
           
        </div>
      </div>
    </div>
  </div>
</div>";
}
function setID($contact){
    $id= $contact->getEmergencyContactId();
}

/*if ($_SERVER['REQUEST_METHOD'] == 'POST')
{

    require("/home/jacadeve/public_html/Joseph/db/db.php");
    if (empty($_POST['first_name'])) {
        $errors[] = 'You forgot to enter your first name.';
        echo "<p>Please enter a emergency first name</p>";

    } else {
        $first_name = mysqli_real_escape_string($dbc, trim($_POST['first_name']));
    }
    if (empty($_POST['last_name'])) {
        $errors[] = 'You forgot to enter a last name.';
        echo "<p>Please enter a emergency last name</p>";
    } else {
        $last_name = mysqli_real_escape_string($dbc, trim($_POST['last_name']));
    }
    // Check for a last name:
    if (empty($_POST['phone']) ) {
        $errors[] = 'You forgot to enter a phone number.';
        echo "<p>Please enter a emergency phone number.</p>";
    } else {

        $tempPhone = preg_replace('/[^0-9]{10,11}/', '', $_POST['phone']);
        //checks is it valid length of the phone number, if yes, it assigns the variable
        if (strlen($tempPhone) >= 10 || strlen($tempPhone) <= 11) {
            $phone = $tempPhone;
        } else {
            $errors[] = "Please enter valid phone number with 10 or 11 numbers.";
        }


        $phone_number = mysqli_real_escape_string($dbc, trim($_POST['phone_number']));
        $phone_number=str_replace("-","",$phone_number);
    }
    if (empty($_POST['alternative_phone'])) {
        $alternative_phone_number=null;
    } else {

        $alternative_phone_number = mysqli_real_escape_string($dbc, trim($_POST['alternative_phone_number']));
        $alternative_phone_number=str_replace("-","",$alternative_phone_number);
    }
    $contID=$_POST['contactID'];

    if (empty($errors)) { // If everything's OK.
        // Register the user in the database...
        // Make the query:

        $q = "UPDATE Clients_Emergency_Contacts `first_name`= $first_name,`last_name`=$last_name,`phone`=$phone_number,`alternate_phone`=$alternative_phone_number WHERE emergency_contact_id=$contID";
        $r = @mysqli_query($dbc, $q); // Run the query.
        if ($r) { // If it ran OK.
            // Print a message:
            $full_name=$first_name.' '.$last_name;
            echo "<p>Thank You, Your information has been posted</p>";
            echo "<p> Emergency Contact Name: $full_name</p>";
            echo "<p> Phone Number: $phone_number";
            echo "<p> Alternative Phone: $alternative_phone_number";
            echo "<p> Conitinue or add another contact</p>";
            echo "<form method='post' action='UserLanding.php'><button type='submit' class='btn btn-primary'>Home</button></form>";

        } else { // If it did not run OK.
            // Public message:
            echo '<h1>System Error</h1>
            <p class="error">You could not be registered due to a system error. We apologize for any inconvenience.</p>';
            // Debugging message:
            echo '<p>' . mysqli_error($dbc) . '<br><br>Query: ' . $q . '</p>';
        } // End of if ($r) IF.
        mysqli_close($dbc); // Close the database connection.
        // Include the footer and quit the script:
    }

}
function updateData($contact){
    echo $contact->getEmergencyContactId();
    require("/Joseph/db/db.php");
    $q = "UPDATE Clients_Emergency_Contacts `first_name`= $_GET[first_name],`last_name`=$_GET[last_name],`phone`=$_GET[phone],`alternate_phone`=$_GET[alternate_phone]WHERE emergency_contact_id=$contact->getEmergencyContactId()";
    $r = @mysqli_query($dbc, $q); // Run the query.
}*/
?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SKCAC Emergency Contact</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="//fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--  <link rel="stylesheet" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">-->
    <link rel="stylesheet" href="../css/dashboard.css">
    <head>
        <script src="jquery-3.3.1.min.js"></script>
    </head>
</head>
<style>
    .header{
        margin-left: 2%;
        margin-top:1%;

    }
    .nav1{
        padding:0px;
    }
    .navitem2{
        float: right;
    }
    .modal {
        vertical-align: center;
    }
</style>
<body>
<nav class="navbar navbar-dark bg-secondary justify-content-between  flex-nowrap nav1 flex-row">
    <div class="bg-dark">
    <a href="UserLanding.php" class="navbar-brand float-left">  <img src="/staff/resources/skcac_header-3.png" height="15%" width="14%">
        SKCAC Industrusties</a>
    </div>
    <div class="navitem2">
        <a class="navbar-brand float-right" href="/staff/signout/index.php">Sign Out</a></li>
        <a href="medications.php" class="navbar-brand float-right">Medical Information</a>

        <a href="updateContact1.1.php" class="navbar-brand float-right">Emergency Contacts</a>

    </div>
</nav>

<main>
    <div class="container-fluid">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pb-2 mb-3 border-bottom">
        <h1><?php echo"$_SESSION[name]"?></h1>
        </div>
        <h1>Emergency Contacts</h1>
        </div>
        <div class="row">
        <?php foreach ( $emergency_contacts as $contact ) {
            render_contact( $contact );
        } ?>


        </div>
</br>
    <a href="EmergenyContact.php"><input class="btn btn-primary" type="submit" value="Add Another Contact"></a>



        <div class="col-md-6 float-center  modal fade" id="edit-contact" tabindex="-1" role="dialog" aria-labelledby="edit-contact-label"
             aria-hidden="true">
            <con class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="edit-contact-label">Edit Contact Information</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form id="contact-edit-form" method="post">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label for="contact-edit-input-first-name" class="input-group-text">First Name</label>
                                    </div>
                                    <input id="contact-edit-input-first-name" name="first_name" type="text" class="form-control"
                                            aria-label="First Name">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label for="contact-edit-input-last-name" class="input-group-text">Last Name</label>
                                    </div>
                                    <input id="contact-edit-input-last-name" name="last_name" type="text" class="form-control"
                                           placeholder="Last Name" aria-label="Last Name">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label for="contact-edit-input-phone" class="input-group-text">Phone Number</label>
                                    </div>
                                    <input id="contact-edit-input-phone" name="phone" type="tel" class="form-control"
                                           placeholder="##########"  aria-label="Phone Number" maxlength="10">
                                </div>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <label for="contact-edit-input-alternate-phone" class="input-group-text">Alternate Phone
                                            Number</label>
                                    </div>
                                    <input id="contact-edit-input-alternate-phone" name="alternate_phone" type="tel"
                                           class="form-control"
                                           placeholder="##########"  aria-label="Alternate Phone Number" maxlength="10">
                                </div>


                            </div>



                    <div class="modal-footer btn-group">

                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-outline-primary " form="contact-edit-form" onclick="<?php setID($contact)?>">Edit Contact</button>
                            </form>
                        </div>
                        </div>
                </div>
                </div>







</main>
</body>

<script>

    $('#edit-contact').on('show.bs.modal', function (event) {
//sessionVariables
        let button = $(event.relatedTarget);
        let contact_id = button.data('contact-id');
        let url = new URL(window.location.href);
        let client_id=button.data('hiddenID'); // From GET
        let modal = $(this);
        alert(client_id);
        alert(contact_id);

        $.ajax({
            url: `/api/client/${client_id}/emergency_contact/${contact_id}`,
            dataType: 'json',
            success: function (data)
            {

                modal.find('#contact-edit-input-first-name').val(data['first_name']);
                modal.find('#contact-edit-input-last-name').val(data['last_name']);
                modal.find('#contact-edit-input-phone').val(data['phone']);
                modal.find('#contact-edit-input-alternate-phone').val(data['alternate_phone']);
                modal.find('#contact-edit-form').attr('action', '/api/client/' + client_id + '/emergency_contact/' + contact_id);

            }
        });
        alert("hello2")
    });

    $(function () {
        $('[data-tooltip="tooltip"]').tooltip()
    });
</script>

<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
    feather.replace()
</script>

<script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>



<script src="/staff/dashboard/js/emergency.js"></script

</html>


